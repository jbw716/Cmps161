// CMPS 161
// Program Assignment 01
// Phillip Weaver
// W0659336

public class Exercise1_1 {

    public static void main(String[] arg) throws InterruptedException {

        System.out.println("Welcome to Java!");
        System.out.println("Welcome to Computer Science!");
        System.out.println("Programming is fun!");

        Thread.sleep(1000);

        System.out.println("\nIntroduce Yourself:");
        System.out.println("Name: Phillip Weaver");
        System.out.println("Hobby: Working on and with computers");
        System.out.println("Major: Information Technology");

        Thread.sleep(1000);

        System.out.println("\nMy 3 classmates.");

        Thread.sleep(1000);

        System.out.println("\nClassmate 1:");
        System.out.println("Name: Dustin Fletcher");
        System.out.println("Hobby: Plays video games");
        System.out.println("Major: Computer Science");

        Thread.sleep(1000);

        System.out.println("\nClassmate 2:");
        System.out.println("Name: Grant Falcon");
        System.out.println("Hobby: Plays guitar");
        System.out.println("Major: Information Technology");

        Thread.sleep(1000);

        System.out.println("\nClassmate 3:");
        System.out.println("Name: Charley");
        System.out.println("Hobby: Enjoys playing video games");
        System.out.println("Major: Information Technology");

        Thread.sleep(1000);

    }
}