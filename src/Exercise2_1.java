// CMPS 161
// Program Assignment 02
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Exercise2_1 {

    public static void main(String[] arg) throws InterruptedException{

        Converter();

    }

    public static void Converter() throws InterruptedException {

        //Enter a temperature in Celsius

        System.out.print("Please enter a temperature in degrees Celsius.\n>");

        Scanner input = new Scanner(System.in);

        try {

            double celsius = input.nextDouble();

            //Convert Celsius to Fahrenheit

            double fahrenheit = ((9.0/5.0) * celsius + 32);

            //Display the result

            System.out.print("\n" + celsius + " degrees Celsius is equal to " + fahrenheit + " degrees Fahrenheit.\n");

            TimeUnit.SECONDS.sleep(1);

        }

        catch(InputMismatchException exception){

            System.out.print("\nOops! That's not a valid input. Please try again.\n");

            //TimeUnit.MILLISECONDS.sleep(500);

            //System.out.print("\nError: " + exception + "\n");

            TimeUnit.SECONDS.sleep(1);

            System.out.print("\n");

            input.nextLine();

            Converter();

        }
    }

}

/* Sample Run
    Enter a temperature in Celsius: 43
    43.0 Celsius in 109.4 Fahrenheit.
 */
