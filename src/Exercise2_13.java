// CMPS 161
// Program Assignment 03
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

//Yearly interest rate is 5%. Monthly is 0.05/12 = 0.00417

//Enter monthly saving: a positive integer

//Original account value

//After the first month account value

//After the second month account value

//After the third month account value

//After the fourth month account value

//After the sixth month account value

//Display the sixth month account value

import java.util.Scanner;

import java.util.concurrent.TimeUnit;

public class Exercise2_13 {

    public static double doubleDeposit;

    public static double percent;

    public static int monthNum;

    public static Scanner scannerInput = new Scanner(System.in);

    public static void main(String[] arg) throws InterruptedException {

        int month = 0;

        double calc = 0;

        doubleDepositVar();

        percentVar();

        double interest = 1 + ((percent/100)/12);

        monthNumVar();

        System.out.print("\n");

        for (int count = 0; count < monthNum; count++) {

            month++;

            calc = (doubleDeposit + calc) * interest;

            System.out.print("Month " + month + ": $" + (Math.round(calc*100.0)/100.0) + "\n");

            TimeUnit.MILLISECONDS.sleep(500);

        }

        TimeUnit.MILLISECONDS.sleep(500);

        System.out.println("\nAfter " + monthNum + " months, the account value would be $" + (Math.round(calc*100.0)/100.0) + ".");

    }

    public static void doubleDepositVar() throws InterruptedException {

        System.out.print("\nHow much money do you wish to deposit per month?\n>");

        try {

            String userInput = scannerInput.nextLine();

            if (userInput.matches(".*[a-z A-Z]+.*|.*[-]+.*")){

                System.out.print("\nOops! That is not a valid input. Please try again!\n");

                TimeUnit.SECONDS.sleep(1);

                doubleDepositVar();

            }

            else {

                doubleDeposit = Double.parseDouble(userInput);

            }

        }

        catch (NumberFormatException exception){

            System.out.print("\nOops! That is not a valid input. Please try again!\n");

            TimeUnit.SECONDS.sleep(1);

            doubleDepositVar();

        }

    }

    public static void percentVar() throws InterruptedException {

        System.out.print("\nHow much yearly interest gain (percent) do you wish to calculate per month?\n>");

        try {

            String userInput = scannerInput.nextLine();

            if (userInput.matches(".*[a-z A-Z]+.*|.*[-]+.*")){

                System.out.print("\nOops! That is not a valid input. Please try again!\n");

                TimeUnit.SECONDS.sleep(1);

                percentVar();

            }

            else {

                percent = Double.parseDouble(userInput);

            }

        }

        catch (NumberFormatException exception){

            System.out.print("\nOops! That is not a valid input. Please try again!\n");

            TimeUnit.SECONDS.sleep(1);

            percentVar();

        }

    }

    public static void monthNumVar() throws InterruptedException {

        System.out.print("\nFor mow many (whole) months do you wish to calculate the interest for?\n>");

        try {

            String userInput = scannerInput.nextLine();

            if (userInput.matches(".*[a-z A-Z]+.*|.*[-]+.*")){

                System.out.print("\nOops! That is not a valid input. Please try again!\n");

                TimeUnit.SECONDS.sleep(1);

                monthNumVar();

            }

            else {

                monthNum = Integer.parseInt(userInput);

            }

        }

        catch (NumberFormatException exception){

            System.out.print("\nOops! That is not a valid input. Please try again!\n");

            TimeUnit.SECONDS.sleep(1);

            monthNumVar();

        }

    }

}

/*  Sample Run
    Enter the monthly saving amount: 100
    After the sixth month, the account value is $608.8181155768638
 */