// CMPS 161
// Program Assignment 04
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

/*
Page 111
3.19 (Compute the perimeter of a triangle)
Write a program that
    reads three edges for a triangle and
    computes the perimeter if the input is valid.
    Otherwise, display that the input is invalid.

    The input is valid if the sum of every pairs of two edges
        is greater than the remaining edge.
 */

import java.util.InputMismatchException;

import java.util.Scanner;

import java.util.concurrent.TimeUnit;

public class Exercise3_19 {

    public static void main (String[] args) throws InterruptedException {

        perimeter();

    }

    public static void perimeter () throws InterruptedException {

        //Enter three edges: Scanner (System.in)

        //Display results
        //if (the sum of every pairs of two edges is greater than the remaining edges)
        // then Display the perimeter of the triangle
        //else
        //  Display the input is invalid

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter the three edge lengths of your triangle.\n>");

        try {

            double input1 = input.nextDouble();

            double input2 = input.nextDouble();

            double input3 = input.nextDouble();


            System.out.print("\nYou entered lengths " + input1 + ", " + input2 + ", and " + input3 + ".\n");

            TimeUnit.MILLISECONDS.sleep(500);

            if (input1 + input2 > input3 && input2 + input3 > input1 && input1 + input3 > input2) {

                double doublePerimeter = input1 + input2 + input3;

                System.out.print("\nThe perimeter of the triangle is: " + (Math.round(doublePerimeter*100.0)/100.0) + "\n");

            }

            else {

                System.out.print("\nSorry. That is not a valid input. Please try again!\n\n");

                TimeUnit.MILLISECONDS.sleep(500);

                perimeter();

            }
        }

        catch (InputMismatchException e){

            System.out.print("\nSorry. That is not a valid input. Please try again.\n\n");

            TimeUnit.MILLISECONDS.sleep(500);

            perimeter();

        }

        again();

    }

    public static void again() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        TimeUnit.MILLISECONDS.sleep(500);

        System.out.print("\nWould you like to calculate the perimeter of another triangle (y/n)?\n>");

        String yesNo = input.nextLine();

        if (yesNo.toLowerCase().startsWith("y") && !(yesNo.toLowerCase().contains("n"))){

            System.out.print("\n");

            perimeter();

        }

        else if (yesNo.toLowerCase().startsWith("n") && !(yesNo.toLowerCase().contains("y"))){

            System.exit(0);

        }

        else{

            System.out.print("\nThat is not a valid input. Please enter \"y\" or \"n\".\n");

            TimeUnit.MILLISECONDS.sleep(500);

            again();

        }

    }

}

/* Sample Run
    Enter three edge lengths: 3.0 4.0 5.0
    The perimeter of the triangle is 12.0

    Enter three edge lengths: 1.0 2.0 3.0
    Input is invalid. (Those edge lengths cannot form a triangle)
 */