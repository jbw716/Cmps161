// CMPS 161
// Program Assignment 05
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

/*
Page 152
4.8 (Finding the character of an ASCII code)
Write a program that receives an ASCII code
    (an integer between 0 and 127) and displays its character.

Here is a sample run:
    Enter an ASCII code: 69
    The character for ASCII code 69 is E.
*/

import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.concurrent.TimeUnit;

public class Exercise4_8 {

    public static void main(String[] args) throws InterruptedException {

        ascii_calc();

    }

    public static void ascii_calc () throws InterruptedException {

        //Enter an ASCII code

        System.out.print("Please enter an ASCII value. (0-127)\n>");

        //Display Result

        try {

            input_calc();

        }

        catch (InputMismatchException e){

            System.out.println("\nSorry. That is not a valid input.\nPlease try again...\n");

            TimeUnit.MILLISECONDS.sleep(500);

            ascii_calc();

        }

    }

    public static void input_calc() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        int ASCII_Value = input.nextInt();

        if (0 <= ASCII_Value && ASCII_Value <= 127) {

            char ASCII_Result = (char) ASCII_Value;

            System.out.println("\nThe character of the ASCII value " + ASCII_Value + " is \"" + ASCII_Result + "\".");

            TimeUnit.MILLISECONDS.sleep(500);

            again();
        }

        else{

            System.out.print("\nSorry. That is not a valid ASCII code.\nPlease try again...\n\n");

            TimeUnit.MILLISECONDS.sleep(500);

            ascii_calc();

        }

    }

    public static void again() throws InterruptedException {

        Scanner input = new Scanner(System.in);

        System.out.print("\nWould you like to find the character for another ASCII code? (y/n)\n>");

        String again = input.nextLine();

        if (again.toLowerCase().startsWith("y") && !(again.toLowerCase().contains("n"))){

            System.out.print("\n");

            ascii_calc();

        }

        else if (again.toLowerCase().startsWith("n") && !(again.toLowerCase().contains("y"))) {

            System.exit(0);

        }

        else {

            System.out.print("\nSorry. That is not a valid input.\nPlease try again...\n");

            TimeUnit.MILLISECONDS.sleep(500);

            again();

        }

    }

}

/* Sample Run
    Enter an ASCII code: 69
    The character for ASCII code 69 is E
 */