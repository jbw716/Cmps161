// CMPS 161
// Program Assignment 06
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

/*
Page 193
5.11 (Finding numbers divisible by 5 or 6, but not by both)
Write a program that displays all the numbers
    from 100 to 200, ten per line,
    that are divisible by 5 or 6, but not both.
 */

public class Exercise5_11 {

    public static void main (String[] args) {

        double count = 0;
        // for (the numbers from 100 to 200)
            // if (this number is divisible by 5 or 6, but not by both) && count++;
                // Display this number (ten per line): System.out.print
        for (int number = 100; number <= 200; number++){

            System.out.print((number % 5 == 0 ^ number % 6 == 0) ? (count > 0 ? ((count > 0 && count % 10 == 0) ? "\n" : " ") : "") + number : "");

            count = ((number % 5 == 0) ^ (number % 6 == 0)) ? ++count : count;

        }

    }

}

/* Output
    100 102 105 108 110 114 115 125 126 130
    132 135 138 140 144 145 155 156 160 162
    165 168 170 174 175 185 186 190 192 195
    198 200
 */