// CMPS 161
// Program Assignment 07
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.lang.NumberFormatException;

public class Exercise6_2 {

    public static void main(String[] args) throws InterruptedException {

        //boolean end = false;

        while (true) {

            //Enter a positive integer: Scanner(System.in)
            System.out.print("Please enter a positive integer or press 'ENTER' without entering anything to finish.\n>");

            Scanner input = new Scanner(System.in);

            String inputString = input.nextLine();

            if (inputString.toLowerCase().equals(""))

                break;

            else {

                try {

                    int inputInt = Integer.parseInt(inputString);

                    //Call method of sumDigits and then display the result

                    System.out.print("\nThe sum of the digits within you input, " + inputInt + " is " + sumDigits(inputInt) + ".\n");

                    TimeUnit.MILLISECONDS.sleep(500);

                    System.out.print("\n");

                } catch (NumberFormatException e) {

                    System.out.print("\nSorry. That is not a valid input. Please try again...\n\n");

                    TimeUnit.MILLISECONDS.sleep(500);

                }

            }

        }

    }// End of main


    public static int sumDigits(long n){

        int temp = (int)Math.abs(n); //Temp value

        int sum = 0; //The sum of the digits

        // while (loop until all the digits are extracted) {
            // extract a digit (%)
            // add the extracted digit into sum
            // remove the extracted digit (/)
        // }

        while (temp > 0){

            sum += temp % 10;

            temp /= 10;

        }

        // return the sum of the digits

        return sum;


    } // end of sumDigits

} // end of Exercise6_2

/* Sample Run
    Enter a number: 234
    The sum of digits for 234 is 9
 */
