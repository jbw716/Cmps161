// CMPS 161
// Program Assignment 08
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.Scanner;

public class Exercise6_3 {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        // Enter a positive integer: Scanner(System.in)

        System.out.print("Please enter a positive integer, word, or sentence (without punctuation).\n>");

        String userInput = input.nextLine();

        // if (the integer is a palindrome

        if (isPalindrome(userInput) == true) {

            //      Display it is a palindrome

            System.out.print("\n" + userInput + " is a palindrome.\n");

        }

        // else

        else {

            //      Display it is not a palindrome

            System.out.print("\n" + userInput + " is not a palindrome.\n");

        }

    }

    public static boolean isPalindrome (String userInput){

        // return if number is palindrome: call reverse method

        if (reverse(userInput).equalsIgnoreCase(userInput)){

            return true;

        }

        else{

            return false;

        }

    }

        // while (loop until all the digits are extracted)

            // extract a digit (%)

            // add the extracted digit into its reversal

            // remove the extracted digit (/)

    public static String reverse (String stringInput){

        String result = "";

        for (int i = stringInput.length(); i > 0; i--)

            result = result + stringInput.charAt(i-1);

        System.out.println("\nThe reverse of your input is " + result.toLowerCase() + ".");

        return result;

    }

}

/* Sample run
    Enter a positive integer: 1234321
    1234321 is a palindrome.
 */