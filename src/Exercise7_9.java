// CMPS 161
// Program Assignment 09
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

//Page 278

import java.util.*;

public class Exercise7_9 {

    public static void main(String[] args) throws InterruptedException {

        //double[] numbers = new double[10];

        List<Double> numbersList = new ArrayList<>();

        Scanner input = new Scanner(System.in);

        // Enter ten double numbers: Scanner(System.in)

        /*for (int i = 0; i < numbers.length; i++)

            numbers[i] = input.nextDouble();*/

        int index = 0;

        while(true){

            System.out.print("Please input the numbers you wish to process. Then press 'enter' without entering anything to finish your input.\n>");

            String userInput = input.nextLine();

            Scanner userInputScanner = new Scanner(userInput);

            if (userInput.equals("")) break;

            for(int rounds = 0; true; rounds++, index++) {

                try {

                    double userInputDouble = userInputScanner.nextDouble();

                    numbersList.add(userInputDouble);

                }

                catch (NoSuchElementException e){

                    if (rounds == 0){

                        System.out.print("Sorry. That is not a valid input. Please try again.\n\n");

                        break;

                    }

                    if (rounds > 0) {

                        if (numbersList.get(index-1) == numbersList.get(index-1).intValue()) System.out.print("\n*The last number processed was " + numbersList.get(index-1).intValue() + ". If this is incorrect, please enter the numbers that followed " + numbersList.get(index-1).intValue() + " again. Otherwise, continue with your input.*\n\n");

                        else System.out.print("\n*The last number processed was " + numbersList.get(index-1) + ". If this is incorrect, please enter the numbers that followed " + numbersList.get(index-1) + " again. Otherwise, continue with your input.*\n\n");

                        break;

                    }

                }

            }

        }

        try {

            double[] numbers = new double[numbersList.size()];

            for (int i = 0; i < numbers.length; i++)

                numbers[i] = numbersList.get(i);


            // Call method min and the display the result

            if (min(numbers) == (int) min(numbers))

                System.out.print("\nThe minimum number in that set is " + (int) min(numbers) + ".\n");

            else

                System.out.println("\nThe minimum number in that set is " + min(numbers) + ".\n");

        }

        catch (ArrayIndexOutOfBoundsException e) {

            System.out.print("Sorry. That is not a valid input. Please try again.\n\n");

            main(null);

        }

    }

    public static double min(double[] list){

        double m = list[0]; // m is the smallest element

        // for (i = 1 to list.length - 1)
            // if (m is larger than list[i]
                // list[i] is the new smallest element

        /*for (int i = 1; i <= list.length - 1; i++)

            if (m > list[i])

                m = list[i];*/

        for (double a : list)

            if (m > a)

                m = a;

        // return the smallest element, m
        return m;

    }

}

/* Sample Run:
    Enter ten numbers: 1.9 2.5 3.7 2 1.5 6 3 4 5 2
    The minimum number is 1.5
 */