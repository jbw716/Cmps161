// CMPS 161
// Program Assignment 00
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MathTest {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        int score = 0;

        System.out.print("\nPlease enter the number of questions you want in the test.\n>");

        int qNumber = input.nextInt();

        //MathTestQuestion[] questions = new MathTestQuestion[qNumber];

        List<MathTestQuestion> questions = new ArrayList<>();

        //System.out.print(questions.length);

        input.nextLine();

        for (int index = 0; index < qNumber; index++){

            System.out.print("\nPlease enter the question.\n>");

            String question = ((index + 1) + ". " + input.nextLine());

            System.out.print("\nPlease enter the first possible answer for your question (A).\n>");

            String a = ("   A. " + input.nextLine());

            System.out.print("\nPlease enter the second possible answer for your question (B).\n>");

            String b = ("   B. " + input.nextLine());

            System.out.print("\nPlease enter the third possible answer for your question (C).\n>");

            String c = ("   C. " + input.nextLine());

            System.out.print("\nPlease enter the fourth possible answer for your question (D).\n>");

            String d = ("   D. " + input.nextLine());

            System.out.print("\nPlease enter the letter value of the answer to the question. (A, B, C, or D)\n>");

            String answer = input.nextLine();

            MathTestQuestion test = new MathTestQuestion(question, a, b, c, d, answer);

            questions.add(test);

        }

        MathTestQuestion[] questionsArray = questions.toArray(new MathTestQuestion[questions.size()]);

        for (int index = 0; index < qNumber; index++) {

            System.out.print("\n" + questionsArray[index].question + "\n\n" + questionsArray[index].a + "\n" + questionsArray[index].b + "\n" + questionsArray[index].c + "\n" + questionsArray[index].d + "\n\nPlease enter your answer.\n>");

            String studentAnswer = input.nextLine();

            if (studentAnswer.equalsIgnoreCase(questionsArray[index].answer)) score += (100.0/qNumber);

        }

        System.out.print("\n" + score + "%\n");

    }

}
