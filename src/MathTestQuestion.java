// CMPS 161
// Program Assignment 00
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.Scanner;

class MathTestQuestion {

    public String question;

    public String a;

    public String b;

    public String c;

    public String d;

    public String answer;

    public MathTestQuestion(String question, String a, String b, String c, String d, String answer){

        this.question = question;

        this.a = a;

        this.b = b;

        this.c = c;

        this.d = d;

        this.answer = answer;

    }

}
