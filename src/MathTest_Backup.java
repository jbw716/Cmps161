// CMPS 161
// Program Assignment 04
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MathTest_Backup {

    public static void main(String[] args) throws InterruptedException {

        Scanner input = new Scanner(System.in);

        int score = 0;

        String answer;

        System.out.print("\nPlease enter the number of question you wish to have on this test.");

        int qNumber = input.nextInt();

        String[] questions = new String[qNumber];

        //for (){



        //}

        String question1 = "Insert question here";

            String q1A = "Insert first possible answer here";

            String q1B = "Insert second possible answer here";

            String q1C = "Insert third possible answer here";

            String q1D = "Insert fourth possible answer here";

            String answer1 = "Insert the answer to the first question here";

        String question2 = "Insert question here";

            String q2A = "Insert first possible answer here";

            String q2B = "Insert second possible answer here";

            String q2C = "Insert third possible answer here";

            String q2D = "Insert fourth possible answer here";

            String answer2 = "Insert the answer to the second question here";

        String question3 = "Insert question here";

            String q3A = "Insert first possible answer here";

            String q3B = "Insert second possible answer here";

            String q3C = "Insert third possible answer here";

            String q3D = "Insert fourth possible answer here";

            String answer3 = "Insert the answer to the third question here";

        String question4 = "Insert question here";

            String q4A = "Insert first possible answer here";

            String q4B = "Insert second possible answer here";

            String q4C = "Insert third possible answer here";

            String q4D = "Insert fourth possible answer here";

            String answer4 = "Insert the answer to the fourth question here";

        String question5 = "Insert question here";

            String q5A = "Insert first possible answer here";

            String q5B = "Insert second possible answer here";

            String q5C = "Insert third possible answer here";

            String q5D = "Insert fourth possible answer here";

            String answer5 = "Insert the answer to the fifth question here";

        String question6 = "Insert question here";

            String q6A = "Insert first possible answer here";

            String q6B = "Insert second possible answer here";

            String q6C = "Insert third possible answer here";

            String q6D = "Insert fourth possible answer here";

            String answer6 = "Insert the answer to the sixth question here";

        String question7 = "Insert question here";

            String q7A = "Insert first possible answer here";

            String q7B = "Insert second possible answer here";

            String q7C = "Insert third possible answer here";

            String q7D = "Insert fourth possible answer here";

            String answer7 = "Insert the answer to the seven question here";

        String question8 = "Insert question here";

            String q8A = "Insert first possible answer here";

            String q8B = "Insert second possible answer here";

            String q8C = "Insert third possible answer here";

            String q8D = "Insert fourth possible answer here";

            String answer8 = "Insert the answer to the eight question here";

        String question9 = "Insert question here";

            String q9A = "Insert first possible answer here";

            String q9B = "Insert second possible answer here";

            String q9C = "Insert third possible answer here";

            String q9D = "Insert fourth possible answer here";

            String answer9 = "Insert the answer to the nine question here";

        String question10 = "Insert question here";

            String q10A = "Insert first possible answer here";

            String q10B = "Insert second possible answer here";

            String q10C = "Insert third possible answer here";

            String q10D = "Insert fourth possible answer here";

            String answer10 = "Insert the answer to the tenth question here";

        System.out.print("\nHello, and welcome to the math practice test.\n");

        TimeUnit.SECONDS.sleep(1);

        System.out.print("\nIn this test, you will be given a question and four possible answers (A, B, C, or D).\n");

        TimeUnit.SECONDS.sleep(1);

        System.out.print("\nAfter you are given the question and the possible answers, you will be prompted for an answer.\n");

        TimeUnit.SECONDS.sleep(1);

        System.out.print("\nType in your answer and press enter. At the end of the test, you will be given a percentage grade.\n");

        TimeUnit.SECONDS.sleep(1);

        System.out.print("\nPress 'ENTER' to begin the test.\n");

        input.nextLine();

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer1)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer2)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer3)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer4)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer5)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer6)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer7)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer8)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer9)) score += 10;

        System.out.print("1. What is the sum of 2^6 * 2^9? (Simplify your answer)\n\n   " +

                "A. 2^15\n   " +

                "B. 32,768\n   " +

                "C. 2^54\n   " +

                "D. 576\n\n" +

                "Please enter your answer.\n>");

        answer = input.nextLine();

        if (answer.equalsIgnoreCase(answer10)) score += 10;

        if (score >= 90) System.out.print("\nCongratulations! You earned a score of " + score + "% on the practice test! Good job! :)");

        else System.out.print("\nYou earned a score of " + score + "% on the practice test.");

    }

}
