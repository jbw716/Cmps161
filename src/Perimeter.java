// CMPS 161
// Program Assignment 01
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Perimeter {

    static ArrayList<Integer> dimensions = new ArrayList<Integer>();
    static Scanner user_input = new Scanner(System.in);
    static Scanner unit_scan = new Scanner(System.in);
    static String numbers;
    static String unit;
    static double perimeter;

    public static void main (String[] arg) throws InterruptedException {

        Dimension_input();

    }

    public static void Dimension_input() throws InterruptedException {

        System.out.print("\nThis is a perimeter calculator. To begin, enter the lengths of the sides of your shape (separate the lengths with spaces).\n>");

        perimeter = 0;

        numbers = user_input.nextLine();
        Scanner numbers_scan = new Scanner(numbers);

        while (numbers_scan.hasNextInt()){

            dimensions.add(numbers_scan.nextInt());

        }

        TimeUnit.MILLISECONDS.sleep(250);

        //System.out.println(dimensions);

        //System.out.println(dimensions);

        //System.out.println(dimensions.size());

        //System.out.println(perimeter);

        if (dimensions.size() == 2){

            System.out.println("Oops! A shape can't have 2 sides! Please try again!");

            dimensions.clear();

            TimeUnit.SECONDS.sleep(3);

            Dimension_input();

        }

        else {

            System.out.print("\nPlease enter the unit of measurement you used for the lengths.\n>");

            unit = unit_scan.nextLine();

            for (int i : dimensions){

                perimeter += i;

            }

            System.out.print("\nYou entered " + dimensions.size() + " sides for you shape.");

            TimeUnit.SECONDS.sleep(1);

            System.out.println("\nThe length of the perimeter of your shape is " + perimeter + " " + unit + ".");

            TimeUnit.SECONDS.sleep(1);

        }

    }

}
