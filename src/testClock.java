// CMPS 161
// Program Assignment 01
// Phillip Weaver
// W0659336
// This Program is brought to you by: Coffee!

public class testClock {

    public static void main(String[] args){

        long totalMillisecs = System.currentTimeMillis();

        long totalSecs = totalMillisecs / 1000;

        long currentSec = totalSecs % 60;

        long totalMins = totalSecs / 60;

        long currentMin = totalMins % 60;

        long totalHours = totalMins / 60;

        long currentHour = totalHours % 24;

        String amPM = "AM";

        if (currentHour > 12){

            currentHour -= 12;

            amPM = "PM";

        }

        System.out.println("The current time is " + currentHour + ":" + currentMin + ":" + currentSec + " " + amPM + " GMT.");

        System.out.println(((System.currentTimeMillis()/1000)/60)%60);

        System.out.println("The current GMT minute is: " + System.currentTimeMillis()/1000/60%60);
    }

}
